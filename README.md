# On the effect of existing infrastructure in energy system models
## Abstract
The transition towards renewable energy is leading to an important strain on the energy grids. The question of the 
design and deployment of renewable energy technologies in symbiosis with existing grids and infrastructure is arising.  

While current energy system models mainly focus on the energy transformation system or only investigate the effect on 
one energy vector grid, we present a methodology to characterize different energy vector grids and storage, integrating 
in the multi-energy and multi-sector modeling framework EnergyScope (Moret et al. & Li et al.).  

The characterization of energy grids is achieved through a traditional energy technology modeling approach, integrating 
economic and energetic parameters. The methodology has been applied to a case study of a country with a high existing 
transmission infrastructure density, switching from a fossil fuel-based system to a high share of renewable energy 
deployment.  

The results show that the economic optimum with high shares of renewable energy requires the electric distribution grid
reinforcement of 2.439 GW (+61 %) LV and 4.626 GW (+82 %) MV, with no reinforcement required at transmission level (HV and
EHV). The reinforcement is due to high shares of LV-PV 15.4 GW and MV-wind 20 GW deployment. Without reinforcement, 
additional biomass is required for methane production, which is stored in 4.8 TWh-5.95 TWh methane storage for 
compensation for seasonal intermittency which, using the existing gas infrastructure. In contrast, the hydro storage 
capacity is used at a maximum of 8.9 TWh. Furthermore, this switch towards less efficient technologies is 
compensated by 8.5-9.3 % cost penalty without reinforcement.
## Introduction
This repository contains the model and result data for the corresponding paper.


## Structure

        .
    ├── 01_Model                                            # EnergyScope model (AMPL)
    ├── 02_Data                                             # Results data for the corresponding subsections
    │   ├── 3.1_Grid_reinforcement_in_economic_optimization 
    │   ├── 3.2_The_role_of_grid_reinforcement              
    │   └── 3.3_The-interplay-of-wind-and-pv                 
    ├── 03_Infrastructure-Documentation                      # Infrastructure technologies documentation
    │   ├── 01_Electricity-infrastructure
    │   └── 02_Gas-Infrastructure
    └── README.md


## Authors and acknowledgment
- [**Jonas Schnidrig**](mailto:jonas.schnidrig@epfl.ch): Main author
- **Rachid Cherkaoui**: Validation and Review
- **Yasmine Calisesi**: Validation and Review
- **Manuele Margni**: Study conceptualization
- **François Maréchal**: Study conceptualization


## License
MIT Licence

## Project status
Published in [Frontiers in Energy](https://doi.org/10.3389/fenrg.2023.1164813)
