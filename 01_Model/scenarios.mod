# scenarios

let co2_limit:= 0;

#let f_min['NUCLEAR'] :=1 ;
#let f_max['DIE_STO'] := 0;
let f_max['NUCLEAR'] := 0;
#let f_max['DAC_LT'] := 0;

#let avail['NG_EHP'] := 1e6;
#let avail['ELECTRICITY_EHV'] := 1e6;
let avail['GASOLINE'] := 0;
let avail['DIESEL'] := 0;
let avail['LFO'] := 0;
/*
param ng_limit >= 0 default 0;


subject to ng_utilization:
	 ng_limit = sum {t in PERIODS, l in {"NG_EHP","NG_HP","NG_MP","NG_LP"}, j in TECHNOLOGIES diff STORAGE_TECH: layers_in_out[j,l]>0} (F_Mult_t [j,t]*layers_in_out[j,l]*t_op[t]);
*/
/*
param hydro_potential >= 0 default 0;
subject to hydro_penetration:
	F_Mult['NEW_HYDRO_DAM'] + F_Mult['NEW_HYDRO_RIVER'] = hydro_potential;
*/
/*
param biomass_potential >= 0 default 0;
subject to biomass_penetration:
	sum {t in PERIODS, b in BIOMASS} F_Mult_t[b,t]*t_op[t] = biomass_potential;
*/

subject to constrainte_julien {t in PERIODS}:
	F_Mult_t['NG_EHP',t]*t_op[t] >= 10;